package handlers

import (
	"ntm/ntm_go_api_gateway/api/http"
	us "ntm/ntm_go_api_gateway/genproto/user_service"

	"github.com/gin-gonic/gin"
)

// UserCreate godoc
// @ID user_create
// @Router /v1/user [POST]
// @Summary create user
// @Description for create user
// @Tags user
// @Accept json
// @Produce json
// @Param User body user_service.CreateUserReq true "User"
// @Success 200 {object} http.Response{data=string} "Response data"
// @Failure 500 {object} http.Response{}
func (h *Handler) UserCreate(c *gin.Context) {
	var user = us.CreateUserReq{}

	err := c.ShouldBindJSON(&user)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}
	res, err := h.services.User().Create(c.Request.Context(), &user)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, res)
}
