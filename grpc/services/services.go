package services

import (
	"ntm/ntm_go_api_gateway/config"

	"ntm/ntm_go_api_gateway/genproto/user_service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	User() user_service.UserServiceClient
}

type grpcClients struct {
	userService user_service.UserServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	/*
		NOW WE HAVENT AUTH SERVICE
	*/
	// connAuthService, err := grpc.Dial(
	// 	cfg.AuthServiceHost+cfg.AuthGRPCPort,
	// 	grpc.WithTransportCredentials(insecure.NewCredentials()),
	// )
	// if err != nil {
	// 	return nil, err
	// }

	connGoService, err := grpc.Dial(
		cfg.ServiceHost+cfg.ServiceGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		userService: user_service.NewUserServiceClient(connGoService),
	}, nil
}

func (g *grpcClients) User() user_service.UserServiceClient {
	return g.userService
}
